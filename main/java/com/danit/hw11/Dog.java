package myhomework12.main.java.com.danit.hw11;

import java.io.Serializable;
import java.util.HashSet;

public class Dog extends Pet implements Foul, Serializable {
    Species species = Species.DOG;

    public Dog(String nickname, int age, int trickLevel, HashSet habits) {
        super(nickname, age, trickLevel, habits);
    }

    public Dog(String nickname) {
        super(nickname);
    }

    public Dog() {
    }

    @Override
    public Species getSpecies() {
        return species;
    }

    @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я - " + super.getNickname() + "." + " Я соскучился!");
    }

    @Override
    public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }
}
